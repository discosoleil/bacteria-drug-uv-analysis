#!/usr/bin/env python
# coding: utf-8

"""
Fonctions pour interagir avec le fichier HDF contenant les données des
manips bactéries sur DISCO.

"""

import h5py

def list_experiments(data_in):
    expe = []
    with h5py.File(data_in, 'r') as f:
        for g in f:
            expe += [str(g)]
    return expe


def load_image(data_in, expe_name):
    with h5py.File(data_in, 'r') as f:
        output = f[expe_name]['images'][:]
        
    return output

def load_mask(data_in, expe_name, pos):
    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        output = f[expe_name][pos]['bacteria_mask'][:]
        
    return output

def load_intensity_profile(data_in, expe_name, channel, pos):
    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        output = f[expe_name][pos]['ch%i_bacteria_intensities' % channel][:]
    
    return output
  
def load_area(data_in, expe_name, pos):
    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        output = f[expe_name][pos]['bacteria_area'][:]
        
    return output 

def save_selected_bactos(data_in, expe_name, pos, selected_bactos):
    with h5py.File(data_in, 'a') as f:
        pos = 'POS%i' % pos
        if 'selected_bactos' in f[expe_name][pos]:
            f[expe_name][pos]['selected_bactos'][:] = selected_bactos
        else:
            f[expe_name][pos].create_dataset('selected_bactos', data=selected_bactos)

def save_drug_signal(data_in, expe_name, pos, drug_data):
    with h5py.File(data_in, 'a') as f:
        pos = 'POS%i' % pos
        if 'drug_signal' in f[expe_name][pos]:
            del f[expe_name][pos]['drug_signal']
            
        f[expe_name][pos].create_dataset('drug_signal', data=drug_data)
        
def load_drug_signal(data_in, expe_name, pos):
    output = None
    
    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        
        if 'drug_signal' in f[expe_name][pos]:
            output = f[expe_name][pos]['drug_signal'][:]
            
    return output
    
    
def load_selected_bactos(data_in, expe_name, pos):
    with h5py.File(data_in, 'r') as f:
        pos = 'POS%i' % pos
        if 'selected_bactos' in f[expe_name][pos]:
            output = f[expe_name][pos]['selected_bactos'][:]
        else:
            output = None
            
    return output


def save_control_exp(data_in, expe_name, control_exp):
    with h5py.File(data_in, 'a') as f:
        gp = f[expe_name]
        gp.attrs['control_exp'] = control_exp
        
def load_control_exp(data_in, expe_name):
    with h5py.File(data_in, 'r') as f:
        gp = f[expe_name]
        try:
            output = gp.attrs['control_exp']
        except:
            output = None

    return output