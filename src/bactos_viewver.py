import ipywidgets as widgets
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon
from skimage.measure import find_contours
import pandas as p
from IPython.display import display, HTML
import glob
from lib_bactos_hdf import *
import tempfile
import os
import shutil
import logging
from matplotlib.backend_bases import NavigationToolbar2, Event

OLDHOME = NavigationToolbar2.home
def new_home(self, *args, **kwargs):
    # register a new event
    OLDHOME(self, *args, **kwargs)
    event = Event('home_event', self)
    self.canvas.callbacks.process('home_event', event)

# redine home command
NavigationToolbar2.home = new_home

def get_all_pos_fluo(selected_file, selected_exp, limit_to_selected_bactos=True, positions=None):
    # On calcul la contribution de la drogue au signal fluo
   
    # On choppe toutes les signaux des bonnes bactos pour les deux positions

    if positions is None:
        positions = [0, 1]

    IF1 = []
    IF2 = []
    # print(positions)
    for pos in positions:
        if limit_to_selected_bactos:
            selected_bactos = load_selected_bactos(selected_file, selected_exp, pos)
            if selected_bactos is None:
                logging.info("selected_bactos for pos %i return None, use all bactos" % pos)
                #raise IndexError
        else:
            selected_bactos = None
            
        I0 = load_intensity_profile(selected_file, selected_exp, 0, pos)
        I1 = load_intensity_profile(selected_file, selected_exp, 1, pos)
        
        if selected_bactos is not None:
            I0 = I0[:, selected_bactos]
            I1 = I1[:, selected_bactos]
            
        # LE TRP est sur le channel 1 et le filtre 2 sur le channel 0
        IF1 += [I1]
        IF2 += [I0]
        
    
    return IF1, IF2

def compute_control_ratio(selected_file, ctrl_exp):

    positions = [p for p in range(load_image(selected_file, ctrl_exp).shape[1])]
    IF1c, IF2c = get_all_pos_fluo(selected_file, ctrl_exp, positions=positions)
        
    IF2c = np.hstack(IF2c)
    IF1c = np.hstack(IF1c)

    return IF2c/IF1c


def compute_drug_fluo(mean_ratio, F1, F2):
    Fdrug = F2 - (mean_ratio*F1.T).T
    
    return Fdrug

def create_area_pandas(h5file, experience, position, selected_bactos=None):
    """
    Retourne un tableau pandas pour les aires des
    bactéries pour une expérience et une position données
    """

    # Chargement des bactéries sélectionnées
    if selected_bactos is None:
        selected_bactos = load_selected_bactos(h5file, experience, position)
        
    area = load_area(h5file, experience, position)
    
    if selected_bactos is not None and sum(selected_bactos):
        idx = p.Index([i for i, val in enumerate(selected_bactos) if val],
                      name='Bactos')
        pd_data = p.DataFrame(area[selected_bactos],
                              columns=['Area (µm^2)'],
                              index=idx)

    else:
        pd_data = None

    return pd_data

def create_intensity_pandas(h5file, experience, position, Ifilter,
                            selected_bactos=None, selected_I=None):
    """
    Retourne un tableau avec les intensités de fluorescence
    """
    
    # Chargement des bactéries sélectionnées
    if selected_bactos is None:
        selected_bactos = load_selected_bactos(h5file, experience, position)

    if Ifilter == 1:
        iimg = 1
    elif Ifilter == 2:
        iimg = 0
    else:
        print('Je connais pas ce filtre')
        return None
    
    if selected_I is None:
        selected_I = load_intensity_profile(h5file, experience, iimg,
                                            position)

    if sum(selected_bactos):
        idx = p.Index([i for i, val in enumerate(selected_bactos) if val],
                      name='Bactos')
        pd_table = p.DataFrame(selected_I.T[selected_bactos],
                               columns=['F%i (t=%i)'%(Ifilter, t) for t in range(len(selected_I))],
                               index=idx)
    else:
        pd_table = None

    return pd_table

def create_control_ratio_pandas(h5file, ctrl_experience, positions=None):
    """
    Fait un tableau pandas avec le rapport F2/F1 pour l'exp controle
    """

    if positions is None:
        ctrl_positions = [p for p in range(load_image(h5file, ctrl_experience).shape[1])]
    else:
        ctrl_positions = positions
       
    ratio_ctrl = compute_control_ratio(h5file, ctrl_experience)
    indexes = []
    for pos in ctrl_positions:
        bselected = load_selected_bactos(h5file, ctrl_experience, pos)
        if bselected is not None:
            indexes += [['pos%i-%i'%(pos, i) for i, val in enumerate(bselected) if val]]
        
    try:
        ctrl_index = np.hstack(indexes)
        # print(ctrl_index)
        pd_table = p.DataFrame(ratio_ctrl.T,
                               columns=['F2/F1 (t=%i)'%t for t in range(len(ratio_ctrl))], 
                               index=p.Index(ctrl_index, name='Bactos'))
    except Exception as e:
        print('fail to build control ratio table')
        print(e)
        pd_table = None
        


    return pd_table


def create_idrug_pandas(h5file, experience, position,
                        selected_bactos=None, Idrug=None):
    """
    Creation d'un tableau pandas avec le calcul de la fluo due à la drogue
    """

    # Chargement des bactéries sélectionnées
    if selected_bactos is None:
        selected_bactos = load_selected_bactos(h5file, experience, position)

    if Idrug is None:
        Idrug = load_drug_signal(h5file, experience, position)

    if Idrug is not None and sum(selected_bactos):
        idx = p.Index([i for i, val in enumerate(selected_bactos) if val], 
                      name='Bactos')
        pd_table = p.DataFrame(Idrug.T[selected_bactos],
                               columns=['Idrug (t=%i)'%t for t in range(len(Idrug))],
                               index=idx)
    else:
        pd_table = None

    return pd_table

class BactosViewver():
    
    def __init__(self, data_file=None):
        """
        BactosViewver permet d'explorer les données stockées en hdf5 
        issue du traitement des images de suivi temporelle des 
        bactéries.
        
        Pour créer le fichier hdf5, il faut lancer le notebook de 
        traitemenet des données brutes.
        """
        
        # Les variables 
        self.selected_imgs = None # Va contenir les images 
        self.selected_mask = None # Va contenir les masques des bactéries
        self.selected_bactos = None # Va contenir quelles bactéries sont selectionnées
        self.bactos_colors = None # Va contenir la couleur des bactéries
        self.selected_pos = 0 # La position selectionnée
        self.selected_time = 0 # Le temps selectionnée
        self.source_file = None # Le chemin vers le fichier source (que l'on va copier dans TMP (pour aller plus vite sur la ruche))
        self.selected_file = data_file # Sur quel fichier on travail
        self.cts1 = None # Enregistre les objets contours matplotlib des bactéries pour le filtre 1
        self.cts2 = None # Enregistre les objets contours matplotlib des bactéries pour le filtre 2
        self.bactos_cts1 = None # pour affichage des bacterie
        self.bactos_cts2 = None
        self.bactos_plottexts = []
        self.i1 = None # Les intensités de fluo des bactéries pour le filtre 1
        self.i2 = None # Les intensitées de fluo des bactéries pour le filtre 2
        
        self.globaloutput = widgets.Output()
        # Construction de l'interface graphique
        if self.selected_file is None:
            with self.globaloutput:
                self.build_data_file_selector_gui()
        else:
            with self.globaloutput:
                self.init_bactos_analyser()
    
        display(self.globaloutput)
     
    def reset_var(self):
        # Les variables 
        self.selected_imgs = None # Va contenir les images 
        self.selected_mask = None # Va contenir les masques des bactéries
        self.selected_bactos = None # Va contenir quelles bactéries sont selectionnées
        self.bactos_colors = None # Va contenir la couleur des bactéries
        self.selected_pos = 0 # La position selectionnée
        self.selected_time = 0 # Le temps selectionnée
        self.source_file = None # Le chemin vers le fichier source (que l'on va copier dans TMP (pour aller plus vite sur la ruche))
        self.selected_file = None # Sur quel fichier on travail
        self.cts1 = None # Enregistre les objets contours matplotlib des bactéries pour le filtre 1
        self.cts2 = None # Enregistre les objets contours matplotlib des bactéries pour le filtre 2
        self.bactos_cts1 = None # pour affichage des bacterie
        self.bactos_cts2 = None
        self.bactos_plottexts = []
        self.i1 = None # Les intensités de fluo des bactéries pour le filtre 1
        self.i2 = None # Les intensitées de fluo des bactéries pour le filtre 2
        
    def build_data_file_selector_gui(self):
        """
        construction du gestionnaire de selection de fichier
        """
  
        h5files_in_folder = glob.glob('**/*.h5', recursive=True)
    
        if len(h5files_in_folder) > 0:
            self.fselect = widgets.Dropdown(
                    options=h5files_in_folder,
                    description='Fichiers trouvés:',
                    disabled=False,
                )
            upbtn = widgets.Button(description='Charger')
            upbtn.on_click(self.load_data)
            hb = widgets.HBox((self.fselect, upbtn))
            display(hb)
        else:
            print('Pas de fichier hdf5 trouvé dans le repertoire (ou sous repertoire) ou se trouve le notebook')
        
    def load_data(self, b):
        """
        Chargement des data,
        On fait une copy du fichier dans un fichier temporaire pour travailler 
        dessus en local quand on utilise la ruche c'est beaucoup plus rapide
        """
        
        # Stoque l'image originale (pour avoir sont nom)
        self.source_file = os.path.abspath(self.fselect.value)
        
        # Copy to a tmp file
        self.selected_fid, self.selected_file = tempfile.mkstemp(suffix='.h5')
        shutil.copy2(self.source_file, self.selected_file)

        #print(self.selected_file)
        with self.globaloutput:
            self.globaloutput.clear_output(wait=True)
            self.init_bactos_analyser()
    
    def save_data(self, b):
        """
        Fonction pour renvoyer les données dans le fichier d'origine
        """
        
        if self.source_file is not None:
            # On copy
            shutil.copy2(self.selected_file, self.source_file)
            
            # On supprime le fichier temporaire
            os.close(self.selected_fid)
            os.remove(self.selected_file)
            
            with self.globaloutput:
                self.globaloutput.clear_output(wait=True)
                self.reset_var()
                self.build_data_file_selector_gui()
                
    def init_bactos_analyser(self):
        """
        Construction de l'interface d'analyse des données
        """
        self.init_data()
        self.build_explore_gui()
        self.build_bactos_selector()
        self.init_main_figure()
            
        self.update_bactos_area_minmax()
        # First update of all tables
        self.update_area_table()
        self.update_intensity_tables()
        self.update_control_ratio_table()

    def build_explore_gui(self):
        """
        Fonction qui construit l'interface graphique
        """
        
        # Creation des zones de sorties 
        self.Wimgout = widgets.Output()
        self.WplotAll = widgets.Output()
        self.Woutbactos = widgets.Output(layout=widgets.Layout(width="330px"))
        self.BactosAreaOutput = widgets.Output()
        self.I1output = widgets.Output()
        self.I2output = widgets.Output()
        self.I1overI2output = widgets.Output()
        self.DrugIoutput = widgets.Output()
        self.AllPosPlotOutput = widgets.Output()
        self.AllExpPlotOutput = widgets.Output()
        
        # Creation des selecteurs listes pour choisir les expériences, les positions et l'experience servant
        # de contrôle 
        self.ListExp = widgets.Dropdown(options=self.available_exp, description='Experiment:',
                                        layout=widgets.Layout(width="98%"))
        self.ListPos = widgets.Dropdown(options=self.positions, description='Pos:',
                                        layout=widgets.Layout(width="98%"))
        self.ListCtrl = widgets.Dropdown(options=self.available_exp, description='Control Exp:',
                                         layout=widgets.Layout(width="98%"))
        export_data_btn = widgets.Button(description='Sauvegarde et fermer',
                                         button_style='danger', icon='save',
                                         layout=widgets.Layout(width='98%'))
        export_data_btn.on_click(self.save_data)
        # Chargement du control pour le premier cas
        ctrl_exp = load_control_exp(self.selected_file, self.ListExp.value)

        if ctrl_exp is not None:
            self.ListCtrl.value = ctrl_exp
        else:
            self.ListCtrl.value = self.ListExp.value
            save_control_exp(self.selected_file, self.ListExp.value, self.ListCtrl.value)
            # Need to save the value

        # Creation des sliders pour controler le temps, et les expositions des images 
        self.t=widgets.IntSlider(min=0, max=max(self.times), step=1, description='time:')
        # Les sliders pour l'intensitée des images
        self.F1minmax = widgets.IntRangeSlider(value=[0, 2**16], min=0, max=5000, 
                                          description='F1:',orientation='horizontal')
        self.F2minmax = widgets.IntRangeSlider(value=[0, 2**16], min=0, max=5000, 
                                          description='F2:',orientation='horizontal')
        
        self.bactos_size_slider = widgets.FloatRangeSlider(value=[0.5, 15],
                                                           min=0.5, max=15,
                                                           step=0.1, description='size:')
        self.update_bactos_area_minmax()
        
        self.bactos_dist_slider =  widgets.IntSlider(value=self.get_min_dist(),
                                                     min=0, max=min(self.H, self.W)/2-10,
                                                     step=1, description='border distance:')

        BTNfilters = widgets.Button(description='Filter', button_style='info',
                                    layout=widgets.Layout(width='100%'))
        BTNfilters.on_click(self.on_filter_bactos)
        layoutfilters = widgets.VBox([self.bactos_size_slider, self.bactos_dist_slider, BTNfilters])

        BTNexport = widgets.Button(description='Exporter vers Excel', button_style='success',
                                    layout=widgets.Layout(width='98%'))
        BTNexport.on_click(self.on_export)
        
        # Les observateurs pour liers des actions 
        self.ListExp.observe(self.on_expe_changed, names='value')
        self.ListPos.observe(self.on_pos_changed, names='value')
        self.ListCtrl.observe(self.on_control_changed, names='value')
        self.t.observe(self.on_time_changed, names='value')
        self.F1minmax.observe(self.on_F1minmax_changed, names='value')
        self.F2minmax.observe(self.on_F2minmax_changed, names='value')
        self.bactos_dist_slider.observe(self.on_dist_changed, names='value')
        
        # Le layout
        vselect_data = widgets.VBox([self.ListPos, self.ListCtrl])
        sliders = widgets.HBox([self.t, self.F2minmax, self.F1minmax],
                               layout=widgets.Layout(width="98%"))
        
        
        wimgs = widgets.VBox([self.Wimgout, sliders, self.WplotAll])
        self.accordion = widgets.Accordion(children=[vselect_data, self.Woutbactos, layoutfilters],
                                           layout=widgets.Layout(width="98%"))
        self.accordion.set_title(0, 'Select Pos and Control')
        self.accordion.set_title(1, 'Select bactos')
        self.accordion.set_title(2, 'Filter bactos')
        
        # Création des différent panneaux
        tabs = widgets.Tab() # layout=widgets.Layout(width="75%")
        tabs.children = [wimgs, self.AllPosPlotOutput, self.AllExpPlotOutput, self.BactosAreaOutput,
                         self.I1output, self.I2output, self.I1overI2output, self.DrugIoutput]
        tabs_titles = ('Plot: data explorer', 'Plot: Idrug for all pos', 'Plot: all exp', 'Data: Bactos area',
                       'Data: F2 fluo', 'Data: F1 fluo', 'Data: F2/F1 control', 'Data: Drug fluo')
        
        for i, title in enumerate(tabs_titles):
            tabs.set_title(i, title)

        if self.source_file is None:
            vcontrol = widgets.VBox([self.ListExp, self.accordion],
                                    #layout=widgets.Layout(width="20%")
                                    )
        else:
            vcontrol = widgets.VBox([export_data_btn, self.ListExp, self.accordion, BTNexport],
                                    #layout=widgets.Layout(width="20%")
                                    )
            
        hcontrol = widgets.HBox([vcontrol, tabs])
        self.final_lay = widgets.AppLayout(header=None,
                                           left_sidebar=vcontrol,
                                           center=tabs,
                                           right_sidebar=None,
                                           footer=None)

        tabs.observe(self.on_tab_changed, names='selected_index')
        display(self.final_lay)


    def init_data(self):
        """
        Load les premiere données pour affichage
        """

        self.available_exp = list_experiments(self.selected_file)
        self.selected_imgs = load_image(self.selected_file, self.available_exp[0])
        self.selected_mask = load_mask(self.selected_file, self.available_exp[0], 0)
        bactos_labels = np.unique(self.selected_mask)
        self.selected_bactos = [True for i in range(len(bactos_labels)-1)]
        self.bactos_area = load_area(self.selected_file, self.available_exp[0], 0)
        
        self.i1 = load_intensity_profile(self.selected_file, self.available_exp[0], 0, 0)
        self.i2 = load_intensity_profile(self.selected_file, self.available_exp[0], 1, 0)

        self.positions = [i for i in range(self.selected_imgs.shape[1])]
        self.times = [t for t in range(len(self.selected_imgs))]
       
        self.W = len(self.selected_imgs[0,0,0,:,0,0])
        self.H = len(self.selected_imgs[0,0,0,0,:,0])
        # Besoin de definir et l'extent et l'origin sur les images et les contours 
        # pour les faire correspondre sinon on peut avoir des décallages
        self.EXTENT = (0, self.H, self.W, 0)

        # update bactos contours
        self.extract_bactos_contour_coordinate()
        
    def init_main_figure(self, adjust_minmax=True):
        
        """
        Fonction qui créée le layout de la figure qui permet de 
        voir les bactéries et leurs masques
        """

        try:
            im1 = self.selected_imgs[self.selected_time, self.selected_pos, 0, :, :, 0]
            im2 = self.selected_imgs[self.selected_time, self.selected_pos, 0, :, :, 1]
        except:
            im1 = self.selected_imgs[0, 0, 0, :, :, 0]
            im2 = self.selected_imgs[0, 0, 0, :, :, 1]
            
        # On ferme toutes les figures ouvertes par mpl
        plt.close('bactostime')
        
        with self.WplotAll:
            self.WplotAll.clear_output()
            self.figure = plt.figure('bactostime', figsize=(12,8))
            # figure.canvas.layout.height = '800px' 

        G = plt.GridSpec(2,3)
        ax1 = plt.subplot(G[0])
        self.plimg1 = plt.imshow(im1,
                                 cmap='gray', interpolation='nearest',
                                 extent=self.EXTENT, origin='upper')

        # Add a rectangle
        self.limit_rectangle = plt.Rectangle( (0,0), self.H, self.W, fill=False,
                                              ec='red', linestyle='--')
        self.update_limit_rectangle()
        
        ax1.add_patch(self.limit_rectangle)
        
        plt.title('F2: channel')
        plt.axis('off')

        plt.subplot(G[1], sharex=ax1, sharey=ax1)
        self.plimg2 = plt.imshow(im2, 
                                 cmap='gray', interpolation='nearest', 
                                 extent=self.EXTENT, origin='upper')
        plt.title('F1: channel (TRP)')
        plt.axis('off')

        # Ajust min-max
        if adjust_minmax:
            self.F2minmax.value = [self.selected_imgs[0, 0, 0, :, :, 0].min(), 
                                   self.selected_imgs[0, 0, 0, :, :, 0].max()]
            self.F1minmax.value = [self.selected_imgs[0, 0, 0, :, :, 1].min(), 
                                   self.selected_imgs[0, 0, 0, :, :, 1].max()]
        else:
            self.plimg1.set_clim(self.F2minmax.value)
            self.plimg2.set_clim(self.F1minmax.value)
        
        # On initialise les autres axes des sous-figures
        axp = plt.subplot(G[3])
        plt.subplot(G[4], sharex=axp)
        plt.subplot(G[2])
        plt.subplot(G[5], sharex=axp)

        self.plot_profiles()
        self.display_bactos_contour_fast(force_redraw=True)

        # Connect the update of profiles based on bacteria currently displayed on the image
        self.figure.canvas.mpl_connect('button_release_event', self.update_bactos_inview)
        self.figure.canvas.mpl_connect('home_event', self.update_bactos_inview)
        plt.tight_layout()
       

    def display_bactos_contour_fast(self, force_redraw=False):
        """
        Dessine le contour des bacteries comme une collection de polygones -> plus rapide
        """

        # get figure axes
        ax1, ax2, ax3, ax4, ax5, ax6 = self.figure.get_axes()

        if force_redraw:
            # On efface si il y avait des bacteries d'affichées
            if self.bactos_cts1 is not None:
                self.bactos_cts1.remove()
                self.bactos_cts2.remove()
               
                for t in self.bactos_plottexts:
                    t.remove()


            # Creation des listes en mémoire
            self.bactos_plottexts = [] # pour les texts

            bactos_cts_poly = []
            edcolors = []
            for i, xy in enumerate(self.bactos_contours):

                # Creation des texts
                self.bactos_plottexts += [ax1.text(xy[:,1].mean(),
                                                   xy[:,0].mean(),
                                                   str(i+1),
                                                   color=self.bactos_colors[i],
                                                   clip_on=True,
                                                   bbox=dict(boxstyle="round",
                                                             ec='none',
                                                             fc=(1., 1, 1, 0.2),
                                                             pad=0.1))]

                if not self.selected_bactos[i]:
                    self.bactos_plottexts[-1].set_visible(False)
                    edcolors += ['none']
                else:
                    edcolors += [self.bactos_colors[i]]

                # Creation des dessins contours
                p = Polygon(np.dstack((xy[:,1], xy[:,0]))[0])
                bactos_cts_poly += [p]

            # Creation de la collection de contour
            self.bactos_cts1 = PatchCollection(bactos_cts_poly)
            self.bactos_cts1.set_edgecolors(edcolors)
            self.bactos_cts1.set_facecolors('None')

            self.bactos_cts2 = PatchCollection(bactos_cts_poly)
            self.bactos_cts2.set_edgecolors(edcolors)
            self.bactos_cts2.set_facecolors('None')

            # Ajout de la collection aux axes
            ax1.add_collection(self.bactos_cts1)
            ax2.add_collection(self.bactos_cts2)

        else:

            # Mise a jour de la visibilité
            if self.bactos_cts1 is not None:
                edcolors = [self.bactos_colors[i] if val else 'none' for i, val in enumerate(self.selected_bactos)]
                self.bactos_cts1.set_edgecolors(edcolors)
                self.bactos_cts2.set_edgecolors(edcolors)
                [self.bactos_plottexts[i].set_visible(val) for i, val in enumerate(self.selected_bactos)]

            
    def extract_bactos_contour_coordinate(self):
        """
        Extraction des coordonnées des contours des bacteries
        """
        self.bactos_contours = []
        self.bactos_center = []

        if self.selected_mask is not None:
            bactos_id = np.unique(self.selected_mask)[1:]
            selectm = self.selected_mask.copy()
            for bid in bactos_id:
                selectm[self.selected_mask == bid] = 1
                selectm[self.selected_mask != bid] = 0
                bactos_xy = find_contours(selectm, 0.5)

                self.bactos_contours += [bactos_xy[0]]
                self.bactos_center += [(bactos_xy[0][:,1].mean(), bactos_xy[0][:,0].mean())]

        self.bactos_center = np.array(self.bactos_center)

    def get_bactos_in_view(self, view_axes):
        """Return the list of bacteria displayed on a 2D view of matplotlib
        """
        xlim = view_axes.get_xlim()
        ylim = view_axes.get_ylim()
        bactos_in_view = np.where(np.logical_and(np.logical_and(self.bactos_center[:,0] > xlim[0],
                                                                self.bactos_center[:,1] > ylim[1]),
                                                np.logical_and(self.bactos_center[:,0] < xlim[1],
                                                               self.bactos_center[:,1] < ylim[0])))

        return bactos_in_view[0].astype(int)

    def update_bactos_inview(self, evt):
        """
        Update line style of plot based on bacteria displayed on image
        """
        # Update the lines on the artist depending on zoom
        axF2 = self.figure.axes[2]
        axF1 = self.figure.axes[3]
        axRatio = self.figure.axes[5]
        ax = self.figure.axes[0]
        linesF2 = axF2.get_lines()
        linesF1 = axF1.get_lines()
        linesR = axRatio.get_lines()
        alllines = (linesF2, linesF1, linesR)
        selected_bactos_index = [i for i, val in enumerate(self.selected_bactos) if val]

        # Get the inview bacos indices
        binview = self.get_bactos_in_view(ax)
        for i, sb in enumerate(selected_bactos_index):
            if sb in binview:
                color = self.bactos_colors[sb]
                zorder = 10
                alpha = 1
            else:
                color = "gray"
                zorder = 2
                alpha = 0.2

            for lines in alllines:
                lines[i].set_color(color)
                lines[i].set_zorder(zorder)
                lines[i].set_alpha(alpha)

        for lines in alllines:
            lines[-1].set_zorder(10)

    def display_stack(self, t, pos, z, chan, update_minmax=False):
        self.plimg1.set_data(self.selected_imgs[t, pos, z, :, :, 0])
        self.plimg2.set_data(self.selected_imgs[t, pos, z, :, :, 1])
    

        if update_minmax:
            p0min = self.selected_imgs[:, pos, z, :, :, 0].min()
            p0max = self.selected_imgs[:, pos, z, :, :, 0].max()
            p1min = self.selected_imgs[:, pos, z, :, :, 1].min()
            p1max = self.selected_imgs[:, pos, z, :, :, 1].max()
        
            # Update sliders min-max
            # Update sliders min-max
            if p0min < p0max and p1min<p1max:
                self.F2minmax.value = [max((700, p0min)), min((p0max, 2400))]
                self.F1minmax.value = [max((700, p1min)), min((p1max, 1800))]
                self.plimg1.set_clim(max((700, p0min)), min((p0max, 2400)))
                self.plimg2.set_clim(max((700, p1min)), min((p1max, 1800)))

    def update_bactos_area(self):
        """
        Fonction pour mettre a jour les données des aires des bactéries
        """

        self.bactos_area = load_area(self.selected_file, self.ListExp.value, self.selected_pos)

    def update_bactos_area_minmax(self):
        """
        Fonction pour ajuster les min/max du slider de selection des bacteries
        """

        if sum(self.selected_bactos):
            good_area = [self.bactos_area[i] for i, val in enumerate(self.selected_bactos) if val]
        
            self.bactos_size_slider.value = [min(good_area)-0.1,
                                             max(good_area)+0.1]
        else:
            if len(self.selected_bactos)>0:
                self.bactos_size_slider.value = [min(self.bactos_area)-0.1,
                                                 max(self.bactos_area)+0.1]

    def update_limit_rectangle(self, lmin=None, update_slider=True):
        """
        Mise à jour du rectangle qui delimite la zonne ou on peut selectionner
        des bactos
        """

        if lmin is None:
            lmin = self.get_min_dist()

        if update_slider:
            self.bactos_dist_slider.value = lmin
            
        self.limit_rectangle.set_xy((lmin, lmin))
        self.limit_rectangle.set_width(self.H-2*lmin)
        self.limit_rectangle.set_height(self.W-2*lmin)

    def on_filter_bactos(self, evt):
        """
        Ajuste les bacteries selectionnées en fonction des filtres 
        """

        # Ajustement de l'aire
        amin, amax = self.bactos_size_slider.value
        for i, val in enumerate(self.selected_bactos):
            if self.bactos_area[i] <= amin or self.bactos_area[i] > amax:
                self.selected_bactos[i] = False
            else:
                self.selected_bactos[i] = True

        # Filtre sur la distance
        lmin = self.bactos_dist_slider.value
        self.filter_min_dist(lmin)
        #self.update_limit_rectangle(lmin, False)
        
        #Save the selected bactos to hdf5
        save_selected_bactos(self.selected_file, self.ListExp.value, 
                             self.selected_pos, self.selected_bactos)

        # Update bactos btns
        for i in range(len(self.bactos_btns)):
            # Ne pas faire de mise a jour du gui dans on_bactos_changed
            self.bactos_btns[i]._update_gui = False
            if self.selected_bactos[i]:
                #self.bactos_btns[i].icon = 'check'
                self.bactos_btns[i].value = True
            else:
                #self.bactos_btns[i].icon = ''
                self.bactos_btns[i].value = False
            self.bactos_btns[i]._update_gui = True
            
        # Adjust title of accordion
        tot = len(self.selected_bactos)
        sel = len([i for i, val in enumerate(self.selected_bactos) if val])
        self.accordion.set_title(1, 'Select bactos (%i/%i)' % (sel, tot) )
        
        # Update plot and tables
        self.plot_profiles()
        self.display_bactos_contour_fast()
        self.update_area_table()
        self.update_intensity_tables()
        
        
    def filter_min_dist(self, min_distance=0):
        """
        Filtre les bacteries qui sont a une certain distance (min_distance % de la largeur ou de la longueur) du bord
        """

        Rimg = self.W/self.H
        
        for i, val in enumerate(self.selected_bactos):
            # Si la bacterie est selectionnée alors on test sa distance
            if val:
                bactos_label = i+1
                x, y = np.where(self.selected_mask == bactos_label)
                xb = x.mean()
                yb = y.mean()
    
                if (xb<=min_distance) or (yb<=min_distance) or (xb >= (self.W-min_distance)) or (yb>=(self.H-min_distance)):
                    self.selected_bactos[i] = False

    
    def get_min_dist(self):
        """
        Fonction sui donne la distance minimum 
        """
        
        dmin = max(self.H, self.W)
        for i, val in enumerate(self.selected_bactos):
            if val:
                bactos_label = i+1
                x, y = np.where(self.selected_mask == bactos_label)
                xb = x.mean()
                yb = y.mean()
            
                if xb < dmin:
                    dmin = xb

                elif self.W-xb < dmin:
                    dmin = (self.W-xb)
                    
                if yb < dmin:
                    dmin = yb

                elif self.H-yb < dmin:
                    dmin = (self.H-yb)
                    
        return dmin
    
    
    def update_all_pos_plot(self):
    
        # Clear axes
        plt.close('Plot all positions')

        if max(self.times) > 0:
            self.figure_all_pos = plt.figure('Plot all positions', figsize=(11,6))
            # Creation de deux axes vides
            ax1 = plt.subplot(211)
            ax2 = plt.subplot(212, sharex=ax1)

            ax1.set_xlabel('Temps')
            ax1.set_ylabel('Control ratio\n F2/F1')
            ax1.set_title('Selected control %s' % self.ListCtrl.value)

            ax2.set_xlabel('Temps')
            ax2.set_ylabel('F2-mean(F2ctrl/F1ctrl)*F1')
            ax2.set_title('Fluo due à la drogue')
        

    
            if self.ListExp.value != self.ListCtrl.value and sum(self.selected_bactos):
                # Select all data from lib import funs both positions
                ctrl_ratio = compute_control_ratio(self.selected_file, self.ListCtrl.value)
                IF1, IF2 = get_all_pos_fluo(self.selected_file, self.ListExp.value, positions=self.positions)
                Idrug = compute_drug_fluo(ctrl_ratio.mean(1), np.hstack(IF1), np.hstack(IF2))
                Idrugnorm = Idrug-Idrug[0,:]
        
                ax1.plot(ctrl_ratio)
                ax1.plot(ctrl_ratio.mean(1), 'k--', lw=2)
                ax1.set_xlabel('Temps')
                ax1.set_ylabel('Control ratio\n F2/F1')
                ax1.set_title('Selected control %s' % self.ListCtrl.value)
    
                ax2.plot(Idrugnorm, '--')
                ax2.plot(Idrugnorm.mean(1), 'k', lw=3)
                ax2.set_xlabel('Temps')
                ax2.set_ylabel('Idrug - Idrug(t0)')
                ax2.set_title('Fluo due à la drogue')

                plt.tight_layout()

        else:
            if self.ListExp.value != self.ListCtrl.value:
                self.figure_all_pos = plt.figure('Plot all positions', figsize=(12,10))
                G = plt.GridSpec(3,2, width_ratios=[1, 1/4])
                ax1 = plt.subplot(G[0,1])
                ax2 = plt.subplot(G[0,0])
                ax3 = plt.subplot(G[1,0], sharex=ax2)
                ax4 = plt.subplot(G[2,0], sharex=ax2)

                ax1.set_xlabel('')
                ax1.set_ylabel('Control ratio\n F2/F1')
                ax1.set_title('Selected control %s' % self.ListCtrl.value)

                ax2.set_ylabel('F2-mean(F2ctrl/F1ctrl)*F1')
                ax2.set_title('Fluo due à la drogue')

            else:
                self.figure_all_pos = plt.figure('Plot all positions', figsize=(12,7))
                G = plt.GridSpec(2,1)
                ax3 = plt.subplot(G[0,0])
                ax4 = plt.subplot(G[1,0], sharex=ax3)
                ax2 = None

            ax3.set_ylabel('fluo filtre F1 (TRP)')
            ax4.set_ylabel('fluo filtre F2 (Drogue)')

            for ax in (ax2, ax3, ax4):
                if ax is not None:
                    ax.set_xlabel('Positions')

            pos_colors = np.array([plt.cm.tab10((i) / float(len(self.positions))) for i in range(len(self.positions))])


            # Select all data from lib import funs both positions
            if self.ListExp.value != self.ListCtrl.value:
                ctrl_ratio = compute_control_ratio(self.selected_file, self.ListCtrl.value)
            else:
                ctrl_ratio = None

            IF1, IF2 = get_all_pos_fluo(self.selected_file, self.ListExp.value, positions=self.positions)
            Idrug = []
            good_pos = []
            if ctrl_ratio is not None:
                for i, pos in enumerate(self.positions):
                    tmp_drug = compute_drug_fluo(ctrl_ratio.mean(), IF1[i], IF2[i])[0]
                    if len(tmp_drug) > 0:
                        Idrug += [ tmp_drug ]
                        good_pos += [ pos ]


                ax1.violinplot(ctrl_ratio[0], showmeans = True)
                ax1.set_xticks([])

                try:
                    parts1 = ax2.violinplot(Idrug, positions=[i for i in good_pos], showmeans=True)
                except:
                    parts1 = None
            else:
                parts1 = None
                good_pos = [self.positions[i] for i, IF in enumerate(IF1) if IF.shape[1] > 0]

            parts2 = ax3.violinplot([IF1[p][0] for p in good_pos],
                                    positions=[i for i in good_pos],
                                    showmeans=True)


            parts3 = ax4.violinplot([IF2[p][0] for p in good_pos],
                                        positions=[i for i in good_pos],
                                        showmeans=True)

            for p in (parts1, parts2, parts3):
                if p is not None:
                    for i, pc in enumerate(p['bodies']):
                        pc.set_color(pos_colors[i])
                    for k in ['cbars', 'cmaxes', 'cmeans', 'cmins']:
                        p[k].set_edgecolors(pos_colors)


            for a in (ax2, ax3, ax4):
                if a is not None:
                    a.set_xticks(good_pos)
                   
            plt.tight_layout()
    
    def update_all_exp_plot(self):
        # Clear axis
        plt.close('Plot all experiments')
        
        self.figure_all_exp = plt.figure('Plot all experiments', figsize=(11,5))

        # Calcul du nombre d'expérience avec un contrôle
        # TODO !!!
        
        ax1 = plt.subplot(111)
    
        # Compute all Idrugnorm
        exp_colors = np.array([plt.cm.rainbow((i+1) / float(len(self.available_exp))) for i in range(len(self.available_exp))])
        allexpplot = []
        goodlabels = []
        for i, exp in enumerate(self.available_exp):
            #print(exp)
            ctrl_exp = load_control_exp(self.selected_file, exp)
        
            if ctrl_exp is not None and ctrl_exp != exp:
                exp_all_drug = []
                positions = [p for p in range(load_image(self.selected_file, exp).shape[1])]
                for pos in positions:
                    drug = load_drug_signal(self.selected_file, exp, pos)
                    SelectB = load_selected_bactos(self.selected_file, exp, pos)
                
                    if drug is not None and SelectB is not None and len(SelectB)>0 and sum(SelectB):
                        exp_all_drug += [drug[:, SelectB]]
              
                if len(exp_all_drug) > 0:
                    Idrug = np.hstack(exp_all_drug)
        
                    #ax1.fill_between(range(len(Idn_mean)),
                    #Idn_mean-Idn_std, Idn_mean+Idn_std, ec=None,
                    #fc=exp_colors[i], alpha=0.5)
                    if max(self.times) > 0:
                        Idrugnorm = Idrug - Idrug[0,:]

                        Idn_mean = Idrugnorm.mean(1)
                        Idn_std = Idrugnorm.std(1)

                        ax1.plot(Idn_mean, color=exp_colors[i], label=exp)
                    else:
                        allexpplot += [Idrug[0].tolist()]
                        goodlabels += [exp]

        if len(allexpplot) > 0 and max(self.times) == 0:
            exp_colors = np.array([plt.cm.tab20((i) / float(len(allexpplot))) for i in range(len(allexpplot))])
            parts = ax1.violinplot(allexpplot, positions=[i for i in range(len(allexpplot))], showmeans=True)
            for i, pc in enumerate(parts['bodies']):
                pc.set_color(exp_colors[i])

            for k in ['cbars', 'cmaxes', 'cmeans', 'cmins']:
                parts[k].set_edgecolors(exp_colors)

            ax1.set_xticks([i for i in range(len(goodlabels))])
            labels = ax1.set_xticklabels(goodlabels, rotation=20, ha='right')
            # ax1.grid(axis='x')
            for i, l in enumerate(labels):
                l.set_color(exp_colors[i])
               
        else:
            plt.legend(loc=0)
        
        ax1.set_ylabel('F2-mean(F2ctrl/F1ctrl)*F1')
        plt.tight_layout()


    def update_area_table(self):

        
        self.areatable = create_area_pandas(self.selected_file,
                                            self.ListExp.value,
                                            self.selected_pos,
                                            self.selected_bactos)

        with self.BactosAreaOutput:
            self.BactosAreaOutput.clear_output()
            if self.areatable is not None:
                display(HTML(self.areatable.to_html(decimal=',', notebook=True, index_names=False)))
                
                
    def update_intensity_tables(self):

        self.F2table = create_intensity_pandas(self.selected_file,
                                               self.ListExp.value,
                                               self.selected_pos,
                                               2, self.selected_bactos,
                                               self.i1)
        self.F1table = create_intensity_pandas(self.selected_file,
                                               self.ListExp.value,
                                               self.selected_pos,
                                               1, self.selected_bactos,
                                               self.i2)
        with self.I1output:
            self.I1output.clear_output()
            if self.F2table is not None:
                display(HTML(self.F2table.to_html(decimal=',', notebook=True, index_names=False)))
    
        with self.I2output:
            self.I2output.clear_output()
            if self.F1table is not None:
                display(HTML(self.F1table.to_html(decimal=',', notebook=True, index_names=False)))

    def update_control_ratio_table(self):
        if self.ListExp.value != self.ListCtrl.value:
            self.f2f1table = create_control_ratio_pandas(self.selected_file,
                                                         self.ListCtrl.value)
        else:
            self.f2f1table = None
            
        with self.I1overI2output:
            self.I1overI2output.clear_output()
            if self.f2f1table is not None:
                display(HTML('<h3>Control experiment: %s</h3>' % self.ListCtrl.value + self.f2f1table.to_html(decimal=',', notebook=True, index_names=False)))

            else:
                print('Please select a control which is not the same as the experiment')

                
    def on_bactos_changed(self, evt):
        
        """
        Activer ou desactiver une bactérie
        """
        
        new_state = evt['new']
        old_state = evt['old']

        id_bactos = evt['owner']._bactos_id
        update_gui = evt['owner']._update_gui
        if new_state:
            evt['owner'].icon = 'check'
        else:
            evt['owner'].icon = ''
        
        if update_gui:
            self.selected_bactos[id_bactos] = new_state
            #Save the selected bactos to hdf5
            save_selected_bactos(self.selected_file, self.ListExp.value, 
                                 self.selected_pos, self.selected_bactos)
    
            self.plot_profiles()
            self.display_bactos_contour_fast()
    
            # Adjust title of accordion
            tot = len(self.selected_bactos)
            sel = len([i for i, val in enumerate(self.selected_bactos) if val])
            self.accordion.set_title(1, 'Select bactos (%i/%i)' % (sel, tot) )
    
            #Update tables
            self.update_area_table()
            self.update_intensity_tables()

            #Update limit zone
            self.update_limit_rectangle()
            
    def build_bactos_selector(self):
        """
        Construction de l'outil qui permet de choisir ces bactéries
        """
    
        # On charge les données depuis le fichier hdf5
        self. selected_bactos = load_selected_bactos(self.selected_file, self.ListExp.value,
                                                     self.selected_pos)
    
        # Si c'est la première fois on crée les données
        if self.selected_bactos is None:
            bactos_labels = np.unique(self.selected_mask)
            self.selected_bactos = [True for i in range(len(bactos_labels)-1)]
            save_selected_bactos(self.selected_file, self.ListExp.value, 
                                 self.selected_pos, self.selected_bactos)
        
        self.bactos_colors = np.array([plt.cm.nipy_spectral((i+1) / float(len(self.selected_bactos))) for i in range(len(self.selected_bactos))])
    
    
        self.bactos_btns = [widgets.ToggleButton(value=bool(val), description='Bactos %i' % (i+1), tooltip='Click to select/unselect') for i, val in enumerate(self.selected_bactos)]
    
        htmlsquares = []
        for i in range(len(self.selected_bactos)):
            cb = self.bactos_colors[i] * 255
            htmlsquares += [widgets.HBox((widgets.HTML(value='<svg><rect style="fill:rgb(%i, %i, %i)" x="0" y="0" width="20" height="20"/></svg>' % (cb[0], cb[1], cb[2]), 
                                                       layout=widgets.Layout(width='20px', height='20px')), self.bactos_btns[i]),
                                         layout=widgets.Layout(overflow='hidden'))]
    
        for i in range(len(self.bactos_btns)):
            self.bactos_btns[i]._bactos_id = i
            self.bactos_btns[i]._update_gui = True
            if self.selected_bactos[i]:
                self.bactos_btns[i].icon = 'check'
            
            self.bactos_btns[i].observe(self.on_bactos_changed, names='value')
    
        bactos_grid = widgets.GridBox(htmlsquares, layout=widgets.Layout(grid_template_columns="repeat(2, 150px)", max_height="500px"))
    
        # Adjust title
        tot = len(self.selected_bactos)
        sel = len([i for i, val in enumerate(self.selected_bactos) if val])
        self.accordion.set_title(1, 'Select bactos (%i/%i)' % (sel, tot) )
    
        with self.Woutbactos:
            self.Woutbactos.clear_output()
            display(bactos_grid)

            
    def update_width_height(self):
        """
        update width and height according to loaded images
        """
        self.W = len(self.selected_imgs[0,0,0,:,0,0])
        self.H = len(self.selected_imgs[0,0,0,0,:,0])
        self.bactos_dist_slider.max = min(self.W, self.H)/2 - 10

        # Need to update the extent
        self.EXTENT = (0, self.H, self.W, 0)
        self.plimg1.set_extent(self.EXTENT)
        self.plimg2.set_extent(self.EXTENT)


    def on_expe_changed(self, evt):    
   
        self.Wimgout.clear_output()
        with self.Wimgout:
            print('Loading images')
            selected = evt['new']
            old_selected = evt['old']

            self.selected_pos = 0
            self.selected_imgs = load_image(self.selected_file, selected)
            self.selected_mask = load_mask(self.selected_file, selected,
                                           self.selected_pos)

            #update positions
            self.positions = [p for p in range(self.selected_imgs.shape[1])]
            self.ListPos.options = self.positions
            self.ListPos.value = self.selected_pos

            # Load the control experiment if it as been given
            ctrl_exp = load_control_exp(self.selected_file, selected)
            # print(ctrl_exp)
            if ctrl_exp is not None:
                self.ListCtrl.value = ctrl_exp
            else:
                # Make the default to be the same experiment as control (meaning no control)
                self.ListCtrl.value = selected
                save_control_exp(self.selected_file, selected, self.ListCtrl.value)


            #update width height
            self.update_width_height()

            # update bactos contours
            self.extract_bactos_contour_coordinate()

            # print(selected_file, selected, selected_pos)
            self.i1 = load_intensity_profile(self.selected_file, selected, 0, self.selected_pos)
            self.i2 = load_intensity_profile(self.selected_file, selected, 1, self.selected_pos)

            self.build_bactos_selector()

            # reset border to minimum
            self.update_limit_rectangle()
            
            # update aread
            self.update_bactos_area()
            self.update_bactos_area_minmax()
            
            try:
                self.display_stack(self.selected_time, self.selected_pos, 0, 0, True)
            except Exception as e:
                print('Error to display images')
                print(e)
        

            try:
                self.display_bactos_contour_fast(force_redraw=True)
            except Exception as e:
                print('Error to display bactos contour')
                print(e)
                
            if sum(self.selected_bactos):        
                try:
                    self.plot_profiles()
                except Exception as e:
                    print('Error in plot fluo profiles')
                    print(e)
    
        with self.AllPosPlotOutput:
            try:
                self.update_all_pos_plot()
            except Exception as e:
                print('Error')
                print(e)
        
        
        #Update tables
        self.update_area_table()
        self.update_intensity_tables()
        self.update_control_ratio_table()
    
        self.Wimgout.clear_output()
    
    def on_pos_changed(self, evt):
        global selected_pos, i1, i2, selected_mask
    
        self.selected_pos = evt['new']
    
        # Reload data for this position
        self.selected_mask = load_mask(self.selected_file, self.ListExp.value, self.selected_pos)
        self.i1 = load_intensity_profile(self.selected_file, self.ListExp.value, 0, self.selected_pos)
        self.i2 = load_intensity_profile(self.selected_file, self.ListExp.value, 1, self.selected_pos)

        # update bactos contours
        self.extract_bactos_contour_coordinate()

        # Creation du selecteur de bacteries
        self.build_bactos_selector()

        # reset border to minimum
        self.update_limit_rectangle()
        
        # update aread
        self.update_bactos_area()
        self.update_bactos_area_minmax()
            
        # Display new data
        self.display_stack(self.selected_time, self.selected_pos, 0, 0, True)
        self.display_bactos_contour_fast(force_redraw=True)
        self.plot_profiles()
    
        #Update tables
        self.update_area_table()
        self.update_intensity_tables()
                
    def on_control_changed(self, evt):
        new_ctrl_exp = evt['new']
        save_control_exp(self.selected_file, self.ListExp.value, new_ctrl_exp)
        self.plot_profiles()
    
        # update the table
        self.update_control_ratio_table()


    def plot_profiles(self):
        """
        Fonction pour tracer les profils de fluo
        """

        # Est qu'on est en temps ou en positions
        if max(self.times) == 0:
            istime=False
        else:
            istime = True

        # Get axis from figure object
        ax1, ax2, ax3, ax4, ax5, ax6 = self.figure.get_axes()
        for ax in (ax3, ax4, ax5, ax6):
            ax.clear()

        # Check si on a au moins une bacterie de selectionnée
        if sum(self.selected_bactos):
            if istime:
                [ax3.plot(self.i1[:, i], color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                ax3.plot(self.i1[:, self.selected_bactos].mean(1), 'k--', lw=2)
                ax3.set_xlabel('Temps')

            else:
                #[ax3.plot(0, self.i1[0, i], 'o', color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                ax3.scatter([0]*len(self.i1[0, self.selected_bactos]), self.i1[0, self.selected_bactos],
                            c=self.bactos_colors[self.selected_bactos])
                ax3.plot(self.i1[0, self.selected_bactos].mean(), 'ko', ms=12)
                ax3.set_xlabel('mean in black')

            ax3.set_ylabel('F2 fluo')

            if istime:
                [ax4.plot(self.i2[:, i], color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                ax4.plot(self.i2[:, self.selected_bactos].mean(1), 'k--', lw=2)
                ax4.set_xlabel('Temps')
            else:
                #[ax4.plot(0, self.i2[0, i], 'o', color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                ax4.scatter([0]*len(self.i2[0, self.selected_bactos]), self.i2[0, self.selected_bactos],
                            c=self.bactos_colors[self.selected_bactos])
                ax4.plot(self.i2[0, self.selected_bactos].mean(), 'ko', lw=2, ms=12)
                ax4.set_xlabel('mean in black')

            ax4.set_ylabel('F1 fluo (TRP)')

            #Min-max for static as min(min(F1),min(F2)) and max(max(F1), max(F2))
            if not istime:
                minF1F2 = np.min((self.i1[0, self.selected_bactos].min(), self.i2[0, self.selected_bactos].min())) - 20
                maxF1F2 = np.max((self.i1[0, self.selected_bactos].max(), self.i2[0, self.selected_bactos].max())) + 20

                ax3.set_ylim(minF1F2, maxF1F2)
                ax4.set_ylim(minF1F2, maxF1F2)

        # Plot du ratio du controle
        ratio_ctrl = compute_control_ratio(self.selected_file, self.ListCtrl.value)
        # print("control ratio shape:")
        # print(ratio_ctrl.shape)
        if ratio_ctrl.shape[1] > 0:
            if istime:
                ax5.plot(ratio_ctrl)
                ax5.plot(ratio_ctrl.mean(1), 'k--', lw=2)
                ax5.set_xlabel('Temps')
            else:
                ax5.plot([0]*ratio_ctrl.shape[1], ratio_ctrl[0], 'o', alpha=0.1, mec='None')
                ax5.plot(0, ratio_ctrl.mean(), 'ko', ms=12)
                ax5.set_xlabel('mean in black')
               
            ax5.set_ylabel('Control ratio\n F2/F1')
            ax5.set_title('Selected control %s' % self.ListCtrl.value)
    
        # Plot de la contribution de la drogue calcul àla Julia
        self.DrugIoutput.clear_output()
        if self.ListExp.value != self.ListCtrl.value and sum(self.selected_bactos):
            try:
                F2 = load_intensity_profile(self.selected_file, self.ListExp.value, 0, self.selected_pos)
                F1 = load_intensity_profile(self.selected_file, self.ListExp.value, 1, self.selected_pos)
                Idrug = compute_drug_fluo(ratio_ctrl.mean(1), F1, F2)
                save_drug_signal(self.selected_file, self.ListExp.value, self.selected_pos, Idrug)

                if istime:
                    [ax6.plot(Idrug[:, i], color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                    ax6.set_xlabel('Temps')
                else:
                    #[ax6.plot(0, Idrug[0, i], 'o', color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                    ax6.scatter([0]*len(Idrug[0, self.selected_bactos]), Idrug[0, self.selected_bactos],
                                c=self.bactos_colors[self.selected_bactos])
                    ax6.set_xlabel('')
                   
                ax6.set_ylabel('F2-mean(F2ctrl/F1ctrl)*F1')
                ax6.set_title('Fluo due à la drogue POS%i'%self.selected_pos)
            except Exception as e:
                print('Error', e)


            # Draw the table of the computed value for the Idrug
            with self.DrugIoutput:
                self.drugtable = create_idrug_pandas(self.selected_file, self.ListExp.value,
                                                     self.selected_pos, self.selected_bactos,
                                                     Idrug)
        
                display(HTML(r'$$Idrug = F2 - \rm{mean}\left(\frac{F2_{control}}{F1_{control}} \right) \times F1_{expe}$$' + self.drugtable.to_html(decimal=',', notebook=True, index_names=False)))
    
        else:
            if sum(self.selected_bactos):
                try:
                    ratio = self.i1/self.i2
                    if istime:
                        [ax6.plot(ratio[:, i], color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                        ax6.plot(ratio[:, self.selected_bactos].mean(1), 'k--', lw=2)
                        ax6.set_xlabel('Temps')
                    else:
                        #[ax6.plot(0, ratio[0, i], 'o', color=self.bactos_colors[i]) for i, val in enumerate(self.selected_bactos) if val]
                        ax6.scatter([0]*len(ratio[0, self.selected_bactos]), ratio[0, self.selected_bactos],
                                    c=self.bactos_colors[self.selected_bactos])
                        ax6.plot(ratio[0, self.selected_bactos].mean(), 'ko', ms=12)
                        ax6.set_xlabel('mean in black')
                       
                    ax6.set_ylabel('F2/F1')
                    ax6.set_title('for position POS%i'%self.selected_pos)


                except Exception as e:
                    print("error", e)
                    print(ratio.shape, len(self.selected_bactos))
            
        if istime:
            for a in (ax4, ax5, ax6):
                ax.set_xlim(0, 15)

    def on_time_changed(self, evt):
        """
        update the time varible
        """
    
        self.selected_time = evt['new']
    
        self.display_stack(self.selected_time, self.selected_pos, 0, 0)
    
    
    def on_F1minmax_changed(self, evt):
        """
        update min-max for F1 filter
        """
        new_min, new_max = evt['new']
        self.plimg2.set_clim(new_min, new_max)
    
    
    def on_F2minmax_changed(self, evt):
        new_min, new_max = evt['new']
        self.plimg1.set_clim(new_min, new_max)
    

    def on_dist_changed(self, evt):
        dmin = evt['new']
        self.update_limit_rectangle(dmin, False)
        
    def on_tab_changed(self, evt):
        tabid = evt['new']
    
        if tabid == 0:
            with self.WplotAll:
                self.WplotAll.clear_output(wait=True)
                self.init_main_figure(adjust_minmax=False)
                plt.show(self.figure)
            
        if tabid == 1:
            with self.AllPosPlotOutput:
                self.AllPosPlotOutput.clear_output(wait=True)
                self.update_all_pos_plot()
                plt.show(self.figure_all_pos)
            
        if tabid == 2:
            with self.AllExpPlotOutput:
                self.AllExpPlotOutput.clear_output(wait=True)
                self.update_all_exp_plot()
                plt.show(self.figure_all_exp)

    def on_export(self, btn):
        self.export_to_excel()
        
    def export_to_excel(self):
        """
        Création d'un fichier excel contenant les données
        """

        # Le nom de sortie (au même endroit que le fichier d'origine)
        fout = os.path.abspath(self.source_file)
        fout = os.path.splitext(fout)[0] + '.xlsx'

        with p.ExcelWriter(fout, engine='xlsxwriter') as writer:
            for expe in self.ListExp.options:
                ctrl_exp = load_control_exp(self.selected_file, expe)
                row = -1  # Initialise row à -1 (si pas de bactos en
                          # position 1, ça met la position 2 en haut
                          # de la feuille excel)
                positions = [p for p in range(load_image(self.selected_file, expe).shape[1])]
                srow = 0

                # Test if it's a time or static
                if load_image(self.selected_file, expe).shape[0] > 1:
                    istime = True
                else:
                    istime = False
                   
                for pos in positions:
                    # print(expe, pos, row)
                    # On construit les tableaux pandas
                    selectedB = load_selected_bactos(self.selected_file,
                                                     expe, pos)
                    if selectedB is not None and sum(selectedB):
                        # Charge les data pour l'aire, et les
                        # intensité de fluo des filtres
                        Area = create_area_pandas(self.selected_file,
                                                  expe, pos, selectedB)
                        F1 = create_intensity_pandas(self.selected_file,
                                                     expe, pos, 1, selectedB)
                        F2 = create_intensity_pandas(self.selected_file,
                                                     expe, pos, 2, selectedB)

                        # srow = int(pos*(row+1))
                        srow += int(row+1)
                        if srow == 0:
                            head = True
                        else:
                            head = False
                           

                        # On exporte les tableaux dans Excel
                        row, col = Area.shape

                        #Crop name if too long >31
                        if len(expe)>31:
                            sname = expe[:31]
                        else:
                            sname = expe

                        Area.to_excel(writer, sheet_name=sname,
                                      header=head, startrow=srow)

                        F1.to_excel(writer, sheet_name=sname, index=False,
                                    header=head, startcol=col+2, startrow=srow)
                        row2, col2 = F1.shape
                        F2.to_excel(writer, sheet_name=sname, index=False,
                                    header=head, startcol=col+2+col2+1,
                                    startrow=srow)
                        if ctrl_exp != expe:
                            Idrug = create_idrug_pandas(self.selected_file,
                                                        expe, pos, selectedB)
                            Idrug.to_excel(writer, sheet_name=sname,
                                           index=False, header=head,
                                           startcol=col+2+(col2+1)*2,
                                           startrow=srow)

                            # Add a text to give the control experiment
                            stre = 'Expérience utilisée comme contrôle %s pour calculer Moyenne(F2c(t)/F1c(t))'% ctrl_exp
                            writer.sheets[sname].write_string(0, col+2+(col2+1)*3,
                                                             stre)
                            writer.sheets[sname].write_string(2, col+2+(col2+1)*3,
                                                             'Idrug = F2 - Moyenne(F2c/F1c)*F1')

                        # Add one to row after head has been created
                        if head:
                            row += 1

                # F2F1 contient les deux position
                if selectedB is not None and expe == ctrl_exp:
                    F2F1 = create_control_ratio_pandas(self.selected_file,
                                                       expe, positions)
                    F2F1.to_excel(writer, sheet_name=sname,
                                  index=True, startcol=col+2+(col2+1)*2)
                   
                if row != -1:
                    if istime:
                        # Draw charts:
                        chartF1 = writer.book.add_chart({'type': 'line'})
                        chartF2 = writer.book.add_chart({'type': 'line'})
                        chart3 = writer.book.add_chart({'type': 'line'})
                        # [sheetname, first_row, first_col, last_row, last_col]
                        for line in range(1, srow+row):
                            chartF1.add_series({'values': [sname, line, col+2, line,
                                                           col+2+col2-1]})
                            chartF2.add_series({'values': [sname, line, col+2+col2+1,
                                                           line, col+2+(col2+1)*2-2]})
                            chart3.add_series({'values': [sname, line, col+2+(col2+1)*2,
                                                          line, col+2+(col2+1)*3-2]})

                        for c in (chartF1, chartF2, chart3):
                            c.set_legend({'none': True})
                        
                        # Insert the chart into the worksheet.
                        worksheet = writer.sheets[sname]
                        worksheet.insert_chart(line+5, col+2, chartF1)
                        worksheet.insert_chart(line+5, col+2+col2+1, chartF2)
                        worksheet.insert_chart(line+5, col+2+(col2+1)*2, chart3)
                else:
                    # Si pas de données pour cette expérience on créé
                    # quand même une feuille mais vide
                    if len(expe) > 31:
                        sname = expe[:31]
                    else:
                        sname = expe

                    writer.book.add_worksheet(sname)
