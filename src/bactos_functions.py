# coding: utf-8
"""
Petit bilbiolthèque contenant les fontions pour le traitement des
bactéries avec la méthode Julia 
"""
import re
import json
import glob
import numpy as np
from tifffile import imread
from skimage.feature import match_template
from skimage.filters import gaussian, threshold_yen
from skimage.restoration import denoise_wavelet, estimate_sigma
from skimage.registration import phase_cross_correlation
from skimage.morphology import disk, white_tophat, opening
from skimage.measure import label, regionprops, find_contours
from skimage.segmentation import clear_border, expand_labels


from scipy import ndimage as ndi
import dateutil
import h5py
import datetime
import os 

from joblib import Parallel, delayed

# Pour utiliser StarDist
from csbdeep.utils import normalize
from stardist.models import StarDist2D, Config2D

def load_MM2_images(image_root_path, channel='*', position='*', time='*', z='*'):
    """
    Fonction pour charche les images avec le pattern définit par MM2 pour des 
    fichiers individuels, soit avec format:
    
    img_channel000_position000_time000000000_z000.tif
    
    Parametres:
    -----------
    - image_root_path: string,
        Chemin vers le dossiers racine contenant les sous dossier 
        de positions (PosO, Pos1, etc) qui contiennent les images
        
    - channel, int or '*',
        Numéro du channel que l'on veut ouvrir, si '*' (le défault)
        cela charge toutes les images
        
    - position, int or '*'
        Pour la position, si '*' charge toutes les positions
    
    - time, int or '*'
        Pour le temps, si '*' charge touts les pas de temps
        
    - z, int or '*'
        Pour le z, si '*' charge tous les pas de profondeur
        
    retourne un collection d'image I
    """
    
    mm2_name_pattern = 'Pos{position}/img_channel{channel:03}_position{position:03}_time{time:09}_z{z:03}.tif'
    reMM2pattern = re.compile('channel([0-9]*)_position([0-9]*)_time([0-9]*)_z([0-9]*)')


    # Cas simple du chargement d'une image données 
    if isinstance(channel, int) and isinstance(position, int) and isinstance(time, int) and isinstance(z, int):
        # Chargement de la bonne image
        imgp = mm2_name_pattern.format(position=position,
                                      channel=channel,
                                      time=time,
                                      z=z)
        
        output = io.imread(image_root_path+imgp)
        
    else:
        # Chargement d'une première images et de ses métadonnées pour créer la tableau numpy en mémroire
        all_channels = glob.glob(image_root_path + 'Pos*')
        print("J'ai trouvé %i positions qui sont: %s" % (len(all_channels), str(all_channels)))
    
        # Lecture d'une image première image pour avoir ses dimension
        all_img_pos0 = glob.glob(all_channels[0]+'/*.tif')
        imtmp = imread(all_img_pos0[0])
        imgshape = imtmp.shape
        imgtype = imtmp.dtype
        print(imgshape, imgtype)
    
        # Lecture des metadata
        meta_data = json.load(open(all_channels[0]+'/metadata.txt'))
        summ = meta_data['Summary']
    
        channel_names = summ['ChNames']
        
        # Creation des dimensiosns du tableau final
        # contenant toutes les images
        dimensions = summ['IntendedDimensions']
        maxtime = dimensions['time']
        maxpos = dimensions['position']
        maxz = dimensions['z']
        maxchan = dimensions['channel']
        
        # Le tableaux dims doit être dans l'ordre temps, pos, z, width, height, channel
        dims = []
        
        if time == '*':
            dims += [maxtime]
            str_time = '*'
        else:
            dims += [1]
            maxtime = 1
            str_time = '%09i' % time
        
        if position == '*':
            dims += [maxpos]
            str_pos = '*'
            str_pos1 = '*'
        else:
            dims += [1]
            maxpos = 1
            str_pos = '%03i' % position
            str_pos1 = '%i' % position
        
        if z == '*':
            dims += [maxz]    
            str_z = '*'
        else:
            dims += [1]
            maxz = 1
            str_z = '%03i' % z
        
        dims += imgshape
        
        if channel == '*':
            dims += [maxchan]
            str_chan = '*'
        else:
            dims += [1]
            maxchan = 1
            str_chan = '%03i' % channel
            

        img_fmt = 'Pos'+str_pos1+'/img_channel'+str_chan+'_position'+str_pos+'_time'+str_time+'_z'+str_z+'.tif'
        # print(image_root_path+img_fmt)
        selected_imgpaths = glob.glob(image_root_path+img_fmt)
        output = np.empty(dims, dtype=imgtype)
        # print(output.shape)     
        for imgp in selected_imgpaths:
            tchan, tpos, tt, tz = [int(i) for i in reMM2pattern.findall(imgp)[0]]
            #print(tchan, tpos, tt, tz)
            # Gestion du chargement de position unique (indice a 0 et non a la valeur de l'indice)
            if maxtime == 1:
                tt = 0
            if maxpos == 1:
                tpos = 0
            if maxz == 1:
                tz = 0
            if maxchan == 1:
                tchan = 0
                
            output[tt, tpos, tz, :, :, tchan] = imread(imgp)
                
        
        print(dimensions, channel_names)
        
    return output, meta_data

def load_MM1_images(image_root_path, channel='*', position='*', time='*', z='*'):
    """
    Fonction pour lire les series d'images issues de MM version 1.X
    """
    mm1_name_pattern = 'Pos{pos}/img_{time:09}_{channel}_{z:03}.tif'
    
    # Lecture des metadata
    with open(image_root_path+'/Pos0/metadata.txt') as f:
        meta = json.load(f)
    
    # On choppe les dimension dans les métadata de la position0
    channels = meta['Summary']['ChNames']
    width = meta['Summary']['Width']
    height = meta['Summary']['Height']
    ntime = meta['Summary']['Frames']
    nslices = meta['Summary']['Slices']
    nPositions = meta['Summary']['Positions']
    nchannels = len(channels)

    if channel == '*':
        loopc = [c for c in range(nchannels)]
    else:
        loopc = [channel]
        nchannels = 1
        
    if position == '*':
        loopp = [p for p in range(nPositions)]
    else:
        loopp = [position]
        nPositions = 1
        
    if time == '*':
        loopt = [t for t in range(ntime)]
    else:
        loopt = [time]
        ntime = 1
        
    if z == "*":
        loopz = [zi for zi in range(nslices)]
    else:
        loopz = [z]
        nslices = 1
    
    # On crée le tableau video qui va recevoir les images en mémoire
    dims = (ntime, nPositions, nslices, height, width, nchannels)
    output = np.empty(dims, dtype=np.dtype('uint16'))

    for t in range(ntime):
        for p in range(nPositions):
            for z in range(nslices):
                for c in range(nchannels):
                    name = mm1_name_pattern.format(pos=p,
                                                   time=t,
                                                   channel=channels[c],
                                                   z=z)
                    
                    try:
                        output[t,p,z,:,:,c] = imread(image_root_path+'/'+name)
                    except Exception as e:
                        print("Erreur de lecture de l'image %s" % name)
                        print(e)
                
    

    return output, meta

def find_paramater_in_MMmeta(metadata, parameter, multi=False):
    """
    Extract a given parameter from the metadata, if multi is True then
    this paramater is searched for each images.
    """
    if multi:
        output = []
    else:
        output = None    
        
    for i in metadata:
        if parameter in metadata[i]:
            if multi:
                output += [metadata[i][parameter]]
            else:
                output = metadata[i][parameter]
                break
            
    return output


def register_task(image, refimage, gaussian_size=0, up_factor=20, show_info=False):
    
    if gaussian_size > 0:
        image = gaussian(image, gaussian_size)
        
    dxdyt, error, _ = phase_cross_correlation(refimage,
                                              image,
                                              upsample_factor=up_factor)

    if show_info:
        print('Displacement Error: %0.2e' % (error))
        
    return dxdyt


def register_images(imgs_stack, gaussian_size = 0,
                    register_filter=False, ref_filter=1,
                    move_filter=0, show_info=False,
                    up_factor=20, drift_filter=None, remove_cosmics=True):
    """
    Register images to correct drift in time and between filter 
    
    Remove 5 pixel on border to remove PIXIS weird lines
    
    remove_cosmics: True,
        use an opening filter of disk size(2) to remove cosmics on images before registration
        
    
    """
    
    # Un premier recallage en temps pour chaque positions soit sur pour chaque filtres (si drift_filter=None)
    # Soit sur un seul drift
    if drift_filter is None:
        filters = range(imgs_stack.shape[5])
    else:
        filters = [drift_filter]
        
    for pos in range(imgs_stack.shape[1]):
        dxdy = []
        for img_filter in filters:
            if show_info:
                print('stabilisation de la position pos %i' % pos)
                print('stabilisation du filtre %i' % img_filter)
                
            imgref = imgs_stack[0, pos, 0, 5:-5, 5:-5, img_filter]
            
            if remove_cosmics:
                el = disk(2)
                imgref = opening(imgref, el)
                
            if gaussian_size > 0:
                imgref = gaussian(imgref, gaussian_size)

            # Non parrallel version
            # dxdyt = [register_task(imgs_stack[t,pos,0,:,:,img_filter], imgref, up_factor=up_factor) for t in range(1, len(imgs_stack))]
            # Parralel version
            if remove_cosmics:
                dxdyt = Parallel(n_jobs=-1)(delayed(register_task)(opening(imgs_stack[t,pos,0, 5:-5, 5:-5, img_filter], el), 
                                                                   imgref, up_factor=up_factor) for t in range(1, len(imgs_stack)))
            else:
                dxdyt = Parallel(n_jobs=-1)(delayed(register_task)(imgs_stack[t,pos,0, 5:-5, 5:-5, img_filter], imgref, up_factor=up_factor) for t in range(1, len(imgs_stack)))
            dxdy += dxdyt
            
            if drift_filter is None:
                for i, t in enumerate(range(1, len(imgs_stack))):
                    imgs_stack[t, pos, 0,:,:,img_filter] = ndi.shift(imgs_stack[t,pos,0,:,:,img_filter], dxdyt[i])
                    
        if drift_filter is not None:
            for img_filter in range(imgs_stack.shape[5]):
                if show_info:
                    print('stabilisation de la position pos %i' % pos)
                    print('stabilisation du filtre %i avec le déplacement filtre %i' % (img_filter, drift_filter))
                for ti, t in enumerate(range(1, len(imgs_stack))):
                    imgs_stack[t, pos, 0,:,:,img_filter] = ndi.shift(imgs_stack[t,pos,0,:,:,img_filter], dxdy[ti])
            
    # Un sercond recallage entre les filtres
    # On recale sur le premier temps uniquement et on 
    # applique le même décallage sur toute la serie temporelle
    if register_filter:
        filter_ref = ref_filter
        filter_move = move_filter
        for pos in range(imgs_stack.shape[1]):
            # On prend la médiane sur l'ensemble des pas de temps
            imgref = imgs_stack[0, pos, 0, 50:-50, 50:-50, filter_ref]
            imgreg = imgs_stack[0, pos, 0, 50:-50, 50:-50, filter_move]

            if gaussian_size > 0:
                imgref = gaussian(imgref, gaussian_size)
                imgreg = gaussian(imgreg, gaussian_size)
                
            dxdyt, error, _ = phase_cross_correlation(imgref, imgreg,
                                                      upsample_factor=up_factor)
            
            if show_info:
                print('Error: %0.2e' % (error))
                
        for t in range(0, len(imgs_stack)):
            imgs_stack[t,pos,0,:,:,filter_move] = ndi.shift(imgs_stack[t,pos,0,:,:,filter_move], dxdyt)

    return imgs_stack
    

def bacteria_contour(bacteria_mask):
    """
    Extract bacteria contour from a bacteria mask obtain from segmentation
    
    Parameters
    ----------
    bacteria_mask: 2d array
        Mask which delineates each bacteria from the image
        
    Return
    ------
    list of x, y contours, shape (nbacteria, 2, len(x))
    """
    
    # Get labels of bacteria (exclude 0, it's the background)
    blabels = np.unique(bacteria_mask)[1:]
    contours = np.empty(len(blabels), dtype=object)
    
    for i, lb in enumerate(blabels):
        # Get the coordinate of bacteria contour
        contour = find_contours(bacteria_mask == lb, 0.5)[0]
        ye, xe = contour.T
        contours[i] = np.vstack((ye, xe))
        
    return contours


def bacteria_bg(bacteria_mask, image, expand_distance=10, minmax_ratio=1/4,
                camera_bg=700):
    """
    Compute the bacground value around each bacteria defined by bacteria_mask.
    
    Parameters
    ----------
    bacteria_mask: 2d array
        Mask which delineates each bacteria from the image
        
    image: 2d array (same size as bacteria_mask)
        Image used to extrack the pixel values for background.
        
    expand_distance: int (optional)
        How far from bacteria contour the pixel should be sampled
        to extract the background.
        
    minmax_ratio: float (optional)
        To compute the background the function apply this ratio between the max and min of pixels 
        around expended bacteria contour.
        
    camera_bg: float or int (optional)
        cutoff value of pixel below this value
        
    Return
    ------
    
    Array of background (shape [number_bacteria])
    """
    
    # First expand bacteria mask
    expended = expand_labels(bacteria_mask, distance=expand_distance)
    
    # Get bacteria contour from expended mask
    bcontours = bacteria_contour(expended)
    
    # Get labels of bacteria (exclude 0, it's the background)
    blabels = np.unique(bacteria_mask)[1:]
    
    bg_values = np.empty(len(blabels))
    
    for i in range(len(blabels)):
        
        # Extract pixels of expended contour
        pixels_bg = ndi.map_coordinates(image,
                                        bcontours[i],
                                        mode='nearest')
        pixels_bg[pixels_bg <= camera_bg] = camera_bg
        
        # Smooth the profile
        pxe_smooth = ndi.median_filter(pixels_bg,
                                       size=len(pixels_bg)//3)
        
        # Compute the bacground from this profile
        # Using a threashold based on max-min
        # threshbg = (pxe_smooth.max() - pxe_smooth.min()) * minmax_ratio + pxe_smooth.min()

        # Take the median of signal (with value only below the mean)
        threshbg = pxe_smooth.mean()
        bg = np.median(pxe_smooth[pxe_smooth <= threshbg])

        bg_values[i] = bg
        
    return bg_values


def bacteria_crosssection(bacteria_mask, bacteria_label, image, camera_bg=700):
    """Compute the crosssection pixel profile of a bacteria
    
    Parameters
    ----------
    bacteria_mask: 2d array
        bacteria mask for the image
        
    bacteria_label: int
        the selected bacteria (start at 1, not 0)
        
    image: 2d array
        image on wich to peform pixel crossection of the bacteria
    """
    
    # Trouver le background en temps
    properties = ('orientation', 'centroid', 
                  'minor_axis_length', 'major_axis_length')
    props = regionprops(bacteria_mask,
                        intensity_image=image)
    
    index = bacteria_label - 1
    assert props[index].label == bacteria_label
    
    yc, xc = props[index].centroid
    orientation = props[index].orientation
    x0 = xc - np.cos(orientation) * 1.5 * props[index].minor_axis_length
    x1 = xc + np.cos(orientation) * 1.5 * props[index].minor_axis_length
    y1 = yc - np.sin(orientation) * 1.5 * props[index].minor_axis_length
    y0 = yc + np.sin(orientation) * 1.5 * props[index].minor_axis_length
    
    xcross = np.linspace(x0, x1)
    ycross = np.linspace(y0, y1)
    pixels_cross = np.ma.masked_less_equal(ndi.map_coordinates(image,
                                                               np.vstack((ycross,xcross)),
                                                               mode='nearest'),
                                           camera_bg)
    
    return xcross, ycross, pixels_cross


# Filtres les objets trouvés 
def object_filter(label_image, min_size=0, max_size=100, min_eccentricity=1.0, 
                   max_eccentricity=0.7, px_per_micro=0):
    """
    Filtre des objets sur leur taille et exentricite (0=cerlce, 1=ellipse trés plate)
    
    Si px_per_micro est différent de zero alors les limites d'aires sont données en µm**2 
    sinon en pixel**2
    
    retourne une nouvelle label_image filtrée
    """
    
    # Calcul les proprietée de chaque bactos
    region_props = regionprops(label_image)
    
    # On créé un tableau en mémoire pour les enregistrer
    good_regions = np.zeros_like(label_image, dtype=np.int)
    #print(min_size, max_size)
    #print(min_eccentricity, max_eccentricity)
    for prop in region_props:
        a = prop.area
        if px_per_micro > 0:
            a *= px_per_micro**2
            
        e = prop.eccentricity
        # print(a, e)
        if a > min_size and a < max_size and e > min_eccentricity and e < max_eccentricity:
            # print('selected')
            good_regions += label_image == prop.label
            
        
    good_labels = label(good_regions > 0, background=0)
    
    return good_labels
            
    
    
# Segmentation des bactos
def segment_bactos(image, image_band=1, denoise=True, 
                   min_size=0, max_size=100, max_eccentricity=1.0, 
                   min_eccentricity=0.7, pix_per_micron=0, tophat_size=None):
    """
    Fonction pour segmenter les bactéries
    
    1 -> tophat_size = None -> estiamte size using round(1/sigma * 70)
    2 -> Apply white_top hat with a disk of size tophat_size
    3 -> denoise with denoise_wavelet
    4 -> apply yen threshold
    5 -> remove small/large objects and objects to close to img border
    6 -> label objects
    
    image: l'image pour obtenir faire la segmentation
    image_band: le filtre a utiliser pour la segmentation
    denoise (true/false): permet d'appliquer un filtre deboise_wavelet
    
    min_size: permet de filtrer les objets trouvés avec une taille min
    max_size: permet de filtrer les objets trouvés avec une taille max
    min_eccentricity: permet de filtrer sur l'exentricité min (0 = cercle, 1 = tres elliptique)
    max_eccentricity: permet de filtrer sur l'exentricité max (0 = cercle, 1 = tres elliptique)
    
    pix_per_micron: taille physique d'un pixel (si zero, on reste en pixel), 
                    si différent de zéro les tailles doivent (max_size, min_size)
                    sont alors en µm**2
                    
    tophat_size (None): set the size of the element for the top hat filter, if None the size is estimated from the image noise as
                        round(1/sigma * 100)
    """
    
    imt = image[:,:,image_band]

    if tophat_size is None:
        eps = estimate_sigma(imt)
        tophat_size = int(np.round(1/eps * 70))
        
    imt = white_tophat(imt, disk(tophat_size))
        
    if denoise:
        imt = denoise_wavelet(imt)
        
    #thres = rank.threshold(imt, disk(10))
    thres = threshold_yen(imt)
    masks = imt > thres
    #masks = thres
    
    labels = object_filter(label(masks), min_size, max_size,
                        min_eccentricity, max_eccentricity, pix_per_micron)
    
    # On enlève le bord de l'image
    remove_border = clear_border(labels, buffer_size=2)
    
    labels = label(remove_border > 0, background=0)
    
    return labels


# Use StarDist trained model to segment data
def segment_bactos_stardist(image, image_band=1, model=None):
    """
    Use StarDist trained on TELEMOS bactos to segment bacteria on fluorescence images
    """
    
   
    # Select image 
    img = image[:,:,image_band]

    # Normalise image
    img = normalize(img)

    # Find bactos
    labels, details = model.predict_instances(img)
    
    return labels

    
def compute_label_filter_drift(labels, image_label, image_target, search_window=50):
    """
    Fonction qui calcule le décalage entre deux filtre basé sur la recherche de pattern (chaque label) entre
    les deux filtres.

    
    labels, image contenant les labels des objets segmentés (skimage label function)
    image_label, image d'origine qui a servi à la segementation
    image_target, image pour laquelle on veut estimer la dérive 
    
    Retourne la difference des positions des centres pour chaqu'un des labels
    """
    
    # Remove around the borders
    # labels = labels[50:-50, 50:-50]
    # image_label = image_label[50:-50, 50:-50]
    # image_target = image_target[50:-50, 50:-50]
    
    regprops = regionprops(labels, intensity_image=image_label)
    
    alldx = np.empty(len(regprops))
    alldy = np.empty(len(regprops))
    
    for i, r in enumerate(regprops):
        aa = r.bbox
        cx, cy = r.centroid
    
        # Constrain search +-50pix around center of cell
        sdx = int(cx)-search_window
        sfx = int(cx)+search_window
        sdy = int(cy)-search_window
        sfy = int(cy)+search_window
    
        if sdx < 0:
            sdx = 0
        if sdy < 0:
            sdy = 0
        if sfx > image_label.shape[0]:
            sfx = image_label.shape[0]
        if sfy > image_label.shape[1]:
            sfy = image_label.shape[1]
        
        pattern = image_label[aa[0]:aa[2],aa[1]:aa[3]]
        result = match_template(image_target[sdx:sfx,sdy:sfy], template=pattern, pad_input=True)
        
        xn, yn = np.unravel_index(np.argmax(result), result.shape)
        xn += sdx
        yn += sdy
    
        alldx[i] = xn-cx
        alldy[i] = yn-cy
        
    return alldx, alldy


def register_filter_from_bactos(bactos, images, position=0, filter_ref=1, filter_target=0):
    """
    Shift filter_target using median position of pattern maching 
    from bacteria segmentation on filter_ref.
    
    return images with shifted position for the filter_target
    """
    
    alldx, alldy = compute_label_filter_drift(bactos, 
                                              images[0, position, 0, :, :, filter_ref], 
                                              images[0, position, 0, :, :, filter_target])
    
    mdx = np.median(alldx)
    mdy = np.median(alldy)
    print('Median drift between filter % and filter %i from template matching: (%0.2f, %0.2f)' %(filter_ref, filter_target, mdx, mdy))
    
    for t in range(len(images)):
        images[t, position, 0, :, :, filter_target] = ndi.shift(images[t, position, 0, :, :, filter_target], 
                                                                (-mdx, -mdy))
        
        
    return images

# Aire, perimètre et bounding box sont indépendant du temps
def compute_bactos_properties(labels, images, position=0, pix_per_micron=1,
                              fluo_bg=700, channels = [0,1]):
    """Get data from bacteria mask

    Parameters:
    -----------
    fluo_bg: int or float or 'auto',
        The value of pixel to offset fluorescence of bacteria. Could be
        manually set (usualy pixis 1024b camera has a value of 700 @ 30s).
        If 'auto' is given, the bacground is estimated using and expended
        contour of bacteria. The value is evaluated for each bacteria, each time
        step and each filter.
    """

    regprops = regionprops(labels)
    bactos_area = [r.area * pix_per_micron**2 for r in regprops]
    bactos_perimeter = [r.perimeter * pix_per_micron for r in regprops]
    bactos_bbox = [r.bbox for r in regprops]
    bactos_labels = [r.label for r in regprops]

    if fluo_bg == 'auto':
        process_fluo = True
        el = disk(2)
    else:
        process_fluo = False

    # Tableau de timension [temps, num_bacterie, filter] pour stoquer les intensitées
    bactos_mean_intensity = np.empty((len(images),len(np.unique(labels))-1, len(channels)))
    for t in range(len(images)):
        for chan in channels:
            # print(chan)
            regs = regionprops(labels, intensity_image=images[t, position, 0, :, :, chan])
            for i, r in enumerate(regs):
                bactos_mean_intensity[t,r.label-1, chan] = r.mean_intensity

            if process_fluo:
                # opening filter to remove cosmics!
                imgt = opening(images[t, position, 0, :, :, chan], el)
                fluo_bg = bacteria_bg(labels, imgt)

            bactos_mean_intensity[t, :, chan] = bactos_mean_intensity[t, :, chan] - fluo_bg
                
    return bactos_area, bactos_perimeter, bactos_bbox, bactos_labels, bactos_mean_intensity
    

def extract_bactos(imgs_stack, mask_band=1, denoise=True,
                   bact_size=(0.5, 15), bact_eccent=(0, 1),
                   pix_per_micro=1, use_stardist=False, 
                   stardist_modelname = 'stardistBactosTELEMOS',
                   stardist_modeldir = './stardist/notebook_from_stardist_github/models/',
                   background=700):

    """
    Make mask on selected band for the time=0 and extract intensity
    and area information from masks

    Parameters:
    -----------

    background: int/float or 'auto',
        Value of the background. If "auto" is given the bg is evaluated for each
        bacteria, each time, and each filter.
    """
    
    data = {}

    # Load stardist model if needed
    if use_stardist:
        # Load the trained model 
        conf = None
        stardist_model = StarDist2D(conf, name=stardist_modelname, basedir=stardist_modeldir)
    
    for p in range(imgs_stack.shape[1]):
        print('position %i' %p)
        tmpd = {}
        
        if use_stardist:
            labels = segment_bactos_stardist(imgs_stack[0,p,0], image_band=mask_band,
                                             model=stardist_model)
        else:
            labels = segment_bactos(imgs_stack[0,p,0], image_band=mask_band, denoise=denoise,
                                    min_size=bact_size[0], max_size=bact_size[1], min_eccentricity=bact_eccent[0],
                                    max_eccentricity=bact_eccent[1], pix_per_micron=pix_per_micro)
            
        print('Trouvé %i bactéries' % (np.unique(labels).max()-1))
        bare, bper, bbox, blab, bint = compute_bactos_properties(labels,
                                                                 imgs_stack,
                                                                 position=p,
                                                                 pix_per_micron=pix_per_micro,
                                                                 fluo_bg=background)
    
        for name, d in zip(('area', 'perim', 'labels', 'bbox', 'intens'), (bare, bper, blab, bbox, bint)):
            tmpd[name] = d
        
        tmpd['bmask'] = labels
        data['POS%i' % p] = tmpd.copy()

    return data


def update_bactos_data(bactos_data, imgs_stack, pix_per_micro=1, background=700):
    """Update data from bacteria already segmented, usefull if you change image.

    Parameters:
    -----------

    bactos_data: dict
        Data for bacteria obtainer from "extract_bactos" function

    background: int/float or 'auto'
        Value to remove to intensities of bacteria. If set to 'auto' background
        is estimated around each bacteria, for each time, each filter.
    """
    data = bactos_data.copy()

    for p in range(imgs_stack.shape[1]):
        print('process position %i' % p)
        tmpd = {}
        labels = data['POS%i' % p]['bmask']

        bare, bper, bbox, blab, bint = compute_bactos_properties(labels,
                                                                 imgs_stack,
                                                                 position=p,
                                                                 pix_per_micron=pix_per_micro,
                                                                 fluo_bg=background)

        for name, d in zip(('area', 'perim', 'labels', 'bbox', 'intens'),
                           (bare, bper, blab, bbox, bint)):
            tmpd[name] = d

        data['POS%i' % p].update(tmpd)

    return data

COMPRESSION = "gzip"

def save_to_h5(output_file, imgs_stack, bactos_data, img_metadata, gp_name,
               pix_per_micro):

    with h5py.File(output_file, 'a') as f:
        # Creation d'un groupe avec le meme nom que le fichier (soit le prefix stoquer dans les metadata de MM)
        datadir = gp_name
        # Si le groupe existe on le supprime
        if datadir in f:
            print('Data %s already exist I replace them with this one' % datadir)
            del f[datadir]
        else:
            print('Save data to group %s' %datadir)
            
        f.create_group(datadir)
        gp = f[datadir]
        
        # Creation des attribus
        gp.attrs['PixelSizeUm'] = pix_per_micro
        gp.attrs['dateprocessing'] = str(datetime.datetime.now())
        gp.attrs['channels_names'] = '; '.join(img_metadata['Summary']['ChNames'])
        gp.attrs['image_shape'] = '[time, position, z, row, col, channels]'
        gp.attrs['data_path'] = os.path.abspath(datadir)
        gp.create_dataset('images', data=imgs_stack, compression=COMPRESSION)
        
        # Sauvegarde des données des bactos pour chaque positions
        for p in range(imgs_stack.shape[1]):
            gppos = gp.create_group('POS%i' % p)
            gppos.create_dataset('bacteria_mask', data=bactos_data['POS%i'%p]['bmask'], compression=COMPRESSION)
            gppos.create_dataset('ch0_bacteria_intensities', data=bactos_data['POS%i'%p]['intens'][:,:,0])
            gppos.create_dataset('ch1_bacteria_intensities', data=bactos_data['POS%i'%p]['intens'][:,:,1])
            gppos.create_dataset('bacteria_area', data=bactos_data['POS%i'%p]['area'])
            gppos.create_dataset('bacteria_perimeter', data=bactos_data['POS%i'%p]['perim'])
            gppos.create_dataset('bacteria_bbox', data=bactos_data['POS%i'%p]['bbox'])
            gppos.create_dataset('bactos_labels', data=bactos_data['POS%i'%p]['labels'])


    print('Data saved in %s' % output_file)

    
def run_processing(data_list, nom_sortie, MM_version=2, background='auto'):
    """
    Parameters:
    
    background, int/float or 'auto'
        The value of bacground to be substracted to bacterie (default "auto", means get background around the bacteria!) 
        if a numeriacal value is given this value is used as background
    
    """
    
    for c in data_list:
        # Chargement des données
        datap = c
        last_dir = os.path.normpath(c).split(os.path.sep)[-1]
        print('Process %s, use name %s' % (c, last_dir))
        if MM_version == 1:
            imgs, metas = load_MM1_images(datap, channel='*', position='*', time='*', z=0)
            pix_per_micro = float(find_paramater_in_MMmeta(metas, 'PixelSize_um'))
        else:
            imgs, metas = load_MM2_images(datap, channel='*', position='*', time='*', z=0)
            pix_per_micro = float(find_paramater_in_MMmeta(metas, 'PixelSizeUm'))
            
        assert pix_per_micro is not None, 'pixel size should be defined'
        
        date = dateutil.parser.parse(metas['Summary']['StartTime'])
        last_dir = '%s-%s' % (date.strftime('%d%m%Y'), last_dir)
        print('save with name %s' % last_dir)
        # Stabilisation
    
        print('stabilisation')
        imgs = register_images(imgs, gaussian_size=0, register_filter=True, 
                               show_info=False, up_factor=10, drift_filter=0)
    
        # Extraction des bacteries
        data = {}
        bdata = extract_bactos(imgs, bact_size=(0.5, 15), pix_per_micro=pix_per_micro, 
                               use_stardist=True, stardist_modeldir='../python/stardist/models/')
    
        # Estimation du drift moyen entre les deux filtre par matching entre les bactos du filtre1 sur le filtre0
        for pos in range(imgs.shape[1]):
            print('Resgister filter1 and filter0 for pos %i' % pos)
            imgs = register_filter_from_bactos(bdata['POS%i'%pos]['bmask'], imgs, position=pos)
    
        # Update intensity data on re-registred images
        bdata = update_bactos_data(bdata, imgs, pix_per_micro, background=background)
        
        if len(bdata['POS0']['labels']) + len(bdata['POS1']['labels']) > 0:
            # Sauvegarde
            save_to_h5(nom_sortie, imgs, bdata, metas, last_dir, pix_per_micro)
        else:
            print('Pas de bactéries, pas de sauvegarde')